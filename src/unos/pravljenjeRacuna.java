package unos;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

/**
 *
 * @author Spremo
 */
public class pravljenjeRacuna {
    
// promenljive vezane za MySQL
    static final String URL = "jdbc:mysql://localhost:3306/";
    static final String BS = "skola";
    static final String UN = "magic";
    static final String PASS = "skola27";
    static Connection con = null;
    
// promenjljive vezane za korisnika racuna
    static int brojRacuna;
    static String ime;    
    static String prezime;
    static String adresa;
    static String mesto;
    static String datum;
    static double iznos;
    static int brojRate;
    
    
    static int x,y,s,v,h;
    //static PDDocument doc;
    //static PDPage page;
    static PDPageContentStream content;
    static PDFont font;
    static PDFont font1;
    static double ukupniIznos;
    
    public static void main(String[] args) {
        
    }
    
    public static void racun(){
        try {
            
// konekcija sa MySQL bazom skola
            con = DriverManager.getConnection( URL + BS, UN, PASS);
            
// postavke za stampanje racuna
            x=40;
            h=279;
            y=303;
            s=610-x;
            v=430;
            
// Podaci o firmi preuzeti iz MySQL baze i tabele firmaPodaci            
            
            String query = "SELECT * FROM firmapodaci";
            ResultSet rsF;
            String firma = null;
            String dodatak = null;
            String adresaFirme = null;
            String opstina = null;
            String PIB = null;
            String racunBanke = null;
                try (PreparedStatement psF = con.prepareStatement(query)) {
                    rsF = psF.executeQuery();
                    while (rsF.next()) {
                        firma = rsF.getString(2);
                        dodatak = rsF.getString(3);
                        adresaFirme = rsF.getString(4);
                        opstina = rsF.getInt(5) + " " + rsF.getString(6);
                        PIB = String.valueOf(rsF.getInt(8));
                        racunBanke = rsF.getString(10); 
                    }
                }
                rsF.close();
                
//  TABELA RACUN citane podataka 
// Podaci o korisniku ceo proces cita samo poslednje podatke iz baze racun, tj samo poslednji unos
            try (

                Statement stR = con.createStatement()) {
                ResultSet rsR;
                query = ("SELECT * FROM racun ORDER BY brojRacuna DESC LIMIT 1;");
                rsR = stR.executeQuery(query);
                if (rsR.next()) {
                    brojRacuna = rsR.getInt(1);
                    ime = rsR.getString(2);
                    prezime = rsR.getString(3);
                    adresa = rsR.getString(4);
                    mesto = rsR.getString(5);
                    datum = rsR.getString(6);
                    iznos = rsR.getDouble(7);
                    brojRate = rsR.getInt(8);
                    System.out.println( "Uplatilac je: " + brojRacuna + " " + ime + " " + prezime + " " + adresa + " " + mesto + " " + datum + " " + iznos + " " + brojRate );
                }
                rsR.close();
            }

            
// Racun i datumi
            String fileName;
            if  ( brojRacuna < 10 ) {
                fileName = "Racun00" + brojRacuna + ".pdf";
            } else if (brojRacuna < 100 ) {
                fileName = "Racun0" + brojRacuna + ".pdf";
            } else { fileName = "Racun" + brojRacuna + ".pdf"; }
            
            char c1 = datum.charAt(8);
            char c2 = datum.charAt(9);
            String godina = String.valueOf( c1*10 + c2);
            String datumIzdavanja = datum;
            String datumPrometa = datum;
            String valutaPlacanja = datum;
            
            // Tabela polja podaci
            int rb = 1;
            String usluga = "Engleski jezik";
            String rata = String.valueOf(brojRate);
            int kolicina = 1;
            double cena = iznos;
            
            String imageName1 = "logo.jpg";
            String imageName = "img/memorandumMW.jpg";
            
// KONACNO PRAVLJENJE PDF FAJLA            
            try {
                try (PDDocument doc = new PDDocument()) {
                    PDPage page = new PDPage();
                    doc.addPage(page);
                    //PDFont font = PDTrueTypeFont.loadTTF(doc, new File ("src/Calibri.ttf"));              // verzija fonta Calibri koja ispisuje font bez srskih slova
                    font = PDType0Font.load(doc, new FileInputStream("src/font/Calibri.ttf"), false);     // verzija koja omogucava ispis srpskih slova
                    font1 = PDType0Font.load(doc, new FileInputStream("src/font/Calibri Bold.ttf"), false);
                    PDImageXObject image = PDImageXObject.createFromFile(imageName, doc);
                    content = new PDPageContentStream(doc, page);
                    content.drawImage(image, 0, 0, 595, 780);                           // postavljanje memoranduma kao pozadine
                    // content.beginText();                                                // pocetak upisa teksta
                    
                    content.setFont(font, 14);                                          // odredjivanje fonta i velicine         
                    content.setLeading(16.5f);
                    // ISPIS podataka o Izdavaocu racuna
                    zaglavljeIzdavaoc( firma, dodatak, adresaFirme, opstina, PIB, racunBanke );
                    // ISPIS podataka o uplatiocu
                    zaglavljeUplatioc ( ime, prezime, adresa, mesto );
                    // ISPIS teksta, broja racuna, mesta, datum izdavanja, prometa ..
                    zaglavljeRacun( brojRacuna , opstina, datumPrometa, datumIzdavanja, valutaPlacanja );
                    // pozivanje metoda za iscrtavanje tabele
                    tabela();
                    // podaci o uplati
                    podaciOUplati ( rb, usluga, rata, kolicina, cena );
                    // donji tekst
                    donjiTekst( ukupniIznos , firma);
                    // stampa ukupnog iznosa
                    content.beginText();
                    content.setFont(font1, 15);
                    content.moveTextPositionByAmount( x+16, 288 );
                    content.drawString("ukupno");
                    content.moveTextPositionByAmount( 500-x, 0 );
                    String ui = String.valueOf(ukupniIznos + "0");
                    content.drawString(ui);
                    content.endText();
                    content.close();                                                    // kraj upisa i praznjenje memorije
                    doc.save("c:/4u/Dokumenti/racuni/" + fileName); // snimanje pfg dokumenta
                    // zatvaranje dokumenta i praznjenje memorije
                }
            
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
            
        } catch (SQLException ex) {
            Logger.getLogger(pravljenjeRacuna.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
  
    private static void tabela() throws IOException {
            
            
            content.drawLine(x,   v, s,   v);
            content.drawLine(x, 428, s, 428);
            content.drawLine(x, 405, s, 405);
            content.drawLine(x, 403, s, 403);
            content.drawLine(x, 383, s, 383);
            content.drawLine(x, 363, s, 363);
            content.drawLine(x, 343, s, 343);
            content.drawLine(x, 323, s, 323);
            content.drawLine(x, 303, s, 303);
            content.drawLine(x, 302, s, 302);
            content.drawLine(x, 301, s, 301);
            content.drawLine(x, 281, s, 281);
            content.drawLine(x, 280, s, 280);
            content.drawLine(x,   h, s,   h);
            
            content.drawLine(   x, v,   x, y);
            content.drawLine(  70, v,  70, y);
            content.drawLine( 240, v, 240, y);
            content.drawLine( 280, v, 280, y);
            content.drawLine( 340, v, 340, y);
            content.drawLine( 380, v, 380, y);
            content.drawLine( 470, v, 470, y);
            content.drawLine(   s, v,   s, y);
            
            
            content.drawLine(  x, y,   x, h);
            content.drawLine(x+1, y, x+1, h);
            content.drawLine(x+2, y, x+2, h);
            content.drawLine(468, y, 468, h);
            content.drawLine(469, y, 469, h);
            content.drawLine(470, y, 470, h);
            content.drawLine(  s, y,   s, h);
            content.drawLine(s+1, y, s+1, h);
            content.drawLine(s+2, y, s+2, h);
            
            content.beginText();
            content.setFont(font1, 12);
            content.newLineAtOffset( x+5 , 412 );
            content.drawString(" RB");
            content.newLineAtOffset( x+30 , 0 );
            content.drawString("VRSTA USLUGE");
            content.newLineAtOffset( x+130 , 0 );
            content.drawString("KOLIČINA");
            content.newLineAtOffset( x+172 , 0 );
            content.drawString("UKUPNO");
            content.endText();
            
            content.beginText();
            content.newLineAtOffset( x+210 , 418 );
            content.drawString("JED.");
            content.newLineAtOffset( x+54 , 0 );
            content.drawString("STOPA");
            content.newLineAtOffset( x+15 , 0 );
            content.drawString("JEDINAČNA");
            content.endText();
            
            content.beginText();
            content.newLineAtOffset( x+204, 408 );
            content.drawString("MERA");
            content.newLineAtOffset( x+60 , 0 );
            content.drawString("PDV %");
            content.newLineAtOffset( x+30 , 0 );
            content.drawString("CENA");
            content.endText();
    }
    
    private static void zaglavljeIzdavaoc
        ( String firma, String dodatak, String adresaFirme, String opstina, String PIB, 
          String racunBanke ) throws IOException {
            content.beginText();
            content.moveTextPositionByAmount(x, 680);
            content.drawString(firma);
            content.newLine();
            content.drawString(dodatak);
            content.newLine();
            content.drawString(adresaFirme);
            content.newLine();
            content.drawString(opstina);
            content.newLine();
            content.drawString("PIB                   " + PIB);
            content.newLine();
            content.drawString("broj računa:   " + racunBanke);
            content.endText();
    }
    
    private static void zaglavljeUplatioc ( String ime, String prezime, String adresa, String mesto )
                                            throws IOException {
            String imePrezime = ime + " " + prezime;
       
            content.beginText();
            int pozicijaX = 560 -imePrezime.length()*6;
            content.moveTextPositionByAmount( pozicijaX, 660);    // pozicioniranje teksta u fajlu    // leva donja pozicija je 0,0      
            content.drawString(imePrezime);
            //content.newLine();                              // komanda za prelaz na novu liniju
            content.endText();
            content.beginText();
            pozicijaX = 560 - adresa.length()*6;
            content.moveTextPositionByAmount( pozicijaX, 640);
            content.drawString(adresa);
            content.endText();
            content.beginText();
            pozicijaX = 560 - mesto.length()*6;
            content.moveTextPositionByAmount( pozicijaX, 620);
            content.drawString(mesto);
            content.endText();
    }
    
    private static void zaglavljeRacun( int brojRacuna , String opstina, String datumPrometa, 
        String datumIzdavanja, String valutaPlacanja ) throws IOException {
    
        content.beginText();
        content.setFont(font1, 22);
        content.newLineAtOffset( 180 , 550);
        if ( brojRacuna < 10 ) {
            content.drawString("RAČUN BROJ:   00" + brojRacuna);
            } else if ( brojRacuna < 100 ) {
                content.drawString("RAČUN BROJ:   0" + brojRacuna);
            } else { content.drawString("RAČUN BROJ:   " + brojRacuna); }
                Calendar calendar = new GregorianCalendar();
                int godina = calendar.get(Calendar.YEAR);
                godina = godina - 2000;
                content.drawString( " / " + godina );
        content.endText();
        
        content.setFont(font, 14);
        content.beginText();
        content.newLineAtOffset(x, 520);
        content.drawString("Mesto i datum izdavanja:             ");
        content.drawString(datumIzdavanja);
        content.newLine(); 
        content.drawString("Datum prometa dobara-usluga:  ");
        content.drawString(datumPrometa);
        content.newLine(); 
        content.drawString("Valuta plaćanja:                              ");
        content.drawString(valutaPlacanja);
        content.newLine(); 
        content.drawString("Način plaćanja:                                  virmanski");
        content.endText();
    }
    
    private static double podaciOUplati(int rb, String usluga, String rata, int kolicina, double cena) 
                                            throws IOException {
        String mera = "kom";
    // stampa rb
        content.beginText();
        int pozicijaX = x+12;
        int pozicijaY = 390;
        for ( int i=0; i<rb; i++ ) {
            String ir = String.valueOf(i+1);
            content.newLineAtOffset(pozicijaX, pozicijaY);
            content.drawString(ir);
            pozicijaX = 0;
            pozicijaY = -20;
        }
        content.endText();
    
    // stampa usluge
        content.beginText();
        pozicijaX = x+60;
        pozicijaY = 390;
        for ( int i=0; i<rb; i++ ) {
            int ir = i+1;
            content.newLineAtOffset(pozicijaX, pozicijaY);
            content.drawString(usluga + " " + rata + " rata");
            content.newLineAtOffset( 150, 0);
            content.drawString(mera);
            rata = rata + "I";
            pozicijaX = -150;
            pozicijaY = -20;
        }
        content.endText();
    // stampa kolicine
        content.beginText();
            pozicijaX = x+265;
            pozicijaY = 390;
            for ( int i=0; i<rb; i++ ) {
                content.newLineAtOffset(pozicijaX, pozicijaY);
                String kol = String.valueOf(kolicina);
                content.drawString(kol);
                pozicijaX = 0;
                pozicijaY = -20;
            }
        content.endText();
    // stampa cene
        content.beginText();
            pozicijaX = x+380;
            pozicijaY = 390;           
            for ( int i=0; i<rb; i++ ) {
                int ir = i+1;
                content.newLineAtOffset(pozicijaX, pozicijaY);
                String cen = String.valueOf(cena);
                content.drawString(cen + "0" );
                double ukupno = cena * kolicina;
                content.newLineAtOffset(x+60, 0);
                String uk = String.valueOf(ukupno + "0");
                content.drawString(uk);
                pozicijaX = -100;
                pozicijaY = -20;
                ukupniIznos = ukupniIznos + ukupno;
            }
        content.endText();
        return ukupniIznos;
    }
    
    private static void donjiTekst(double ukupniIznos, String firma) throws IOException {
       content.beginText();
       content.newLineAtOffset(x, 260);
       content.drawString("Slovima:                 " + slovimaIznos(ukupniIznos));
       content.newLine();
       content.drawString("Napomena:           " + firma + " NIJE 'U SISTEMU' PDV-A.");
       content.newLine();
       content.drawString("Poreska olakšica: --- ");
       content.endText();
       
       content.beginText();
       content.newLineAtOffset( x+20, 140);
       content.drawString("   ODGOVORNO LICE");
       content.newLine();
       content.drawString("   PRIMAOCA RAČUNA");
       content.endText();
       
       content.beginText();
       content.newLineAtOffset(465-x, 140);
       content.drawString("       ODGOVORNO LICE");
       content.newLine();
       content.drawString("  IZDAVAOCA RAČUNA");
       content.endText();
       
       content.drawLine( x+30, 80, x+150, 80);
       content.drawLine( x+30, 81, x+150, 81);
       content.drawLine( 460-x, 80, 580-x, 80);
       content.drawLine( 460-x, 81, 580-x, 81);
    }
    
    private static String slovimaIznos( double cifra ) {
        String slovimaIznos = "";
        int h = (int) (cifra / 1000);
        switch(h) {
            case 1: {
                slovimaIznos = slovimaIznos + "jednahiljada";
                break;
            }
            case 2: {
                slovimaIznos = slovimaIznos + "dvehiljade";
                break;
            }
            case 3: {
                slovimaIznos = slovimaIznos + "trihiljade";
                break;
            }
            case 4: {
                slovimaIznos = slovimaIznos + "četirihiljade";
                break;
            }
            case 5: {
                slovimaIznos = slovimaIznos + "pethiljada";
                break;
            }
            case 6: {
                slovimaIznos = slovimaIznos + "šesthiljada";
                break;
            }
            case 7: {
                slovimaIznos = slovimaIznos + "sedamhiljada";
                break;
            }
            case 8: {
                slovimaIznos = slovimaIznos + "osamhiljada";
                break;
            }
            case 9: {
                slovimaIznos = slovimaIznos + "devethiljada";
                break;
            }
            case 10: {
                slovimaIznos = slovimaIznos + "desethiljada";
                break;
            }
        }
        
        int s = (int) ((cifra - h*1000) / 100) ;
        switch(s) {
            case 1: {
                slovimaIznos = slovimaIznos + "jednastotina";
                break;
            }
            case 2: {
                slovimaIznos = slovimaIznos + "dvestotine";
                break;
            }
            case 3: {
                slovimaIznos = slovimaIznos + "tristotine";
                break;
            }
            case 4: {
                slovimaIznos = slovimaIznos + "četiristotine";
                break;
            }
            case 5: {
                slovimaIznos = slovimaIznos + "petstotina";
                break;
            }
            case 6: {
                slovimaIznos = slovimaIznos + "šeststotina";
                break;
            }
            case 7: {
                slovimaIznos = slovimaIznos + "sedamstotina";
                break;
            }
            case 8: {
                slovimaIznos = slovimaIznos + "osamstotina";
                break;
            }
            case 9: {
                slovimaIznos = slovimaIznos + "devetstotina";
                break;
            }
        }
        
        int d = (int) ((cifra - h*1000 - s*100) / 10);
        if ( d > 2 ) {
            switch(d) {
                case 2: {
                    slovimaIznos = slovimaIznos + "dvadeset";
                    break;
                }
                case 3: {
                    slovimaIznos = slovimaIznos + "trideset";
                    break;
                }
                case 4: {
                    slovimaIznos = slovimaIznos + "četrdeset";
                    break;
                }
                case 5: {
                    slovimaIznos = slovimaIznos + "pedeset";
                    break;
                }
                case 6: {
                    slovimaIznos = slovimaIznos + "šezdeset";
                    break;
                }
                case 7: {
                    slovimaIznos = slovimaIznos + "sedamdeset";
                    break;
                }
                case 8: {
                    slovimaIznos = slovimaIznos + "osamdeset";
                    break;
                }
                case 9: {
                    slovimaIznos = slovimaIznos + "devedeset";
                    break;
                }
            }
        }
        
        int j = (int) ((cifra - h*1000 - s*100 - d*10));
        switch(j) {
            case 0: {
                break;
            }
            case 1: {
                slovimaIznos = slovimaIznos + "jedan";
                break;
            }
            case 2: {
                slovimaIznos = slovimaIznos + "dva";
                break;
            }
            case 3: {
                slovimaIznos = slovimaIznos + "tri";
                break;
            }
            case 4: {
                slovimaIznos = slovimaIznos + "četiri";
                break;
            }
            case 5: {
                slovimaIznos = slovimaIznos + "pet";
                break;
            }
            case 6: {
                slovimaIznos = slovimaIznos + "šest";
                break;
            }
            case 7: {
                slovimaIznos = slovimaIznos + "sedam";
                break;
            }
            case 8: {
                slovimaIznos = slovimaIznos + "osam";
                break;
            }
            case 9: {
                slovimaIznos = slovimaIznos + "devet";
                break;
            }
            case 10: {
                slovimaIznos = slovimaIznos + "deset";
                break;
            }
            case 11: {
                slovimaIznos = slovimaIznos + "jedanaest";
                break;
            }
            case 12: {
                slovimaIznos = slovimaIznos + "dvanaest";
                break;
            }
            case 13: {
                slovimaIznos = slovimaIznos + "trinaest";
                break;
            }
            case 14: {
                slovimaIznos = slovimaIznos + "četrnaest";
                break;
            }
            case 15: {
                slovimaIznos = slovimaIznos + "petnaest";
                break;
            }
            case 16: {
                slovimaIznos = slovimaIznos + "šesnaest";
                break;
            }
            case 17: {
                slovimaIznos = slovimaIznos + "sedamnaest";
                break;
            }
            case 18: {
                slovimaIznos = slovimaIznos + "osamnaest";
                break;
            }
            case 19: {
                slovimaIznos = slovimaIznos + "devetnaest";
                break;
            }
            
        }
        slovimaIznos = slovimaIznos + "dinara i nulapara.";
        return slovimaIznos;
    }
}
