
package unos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class isplata {
    
// promenljive vezane za MySQL
    static final String URL = "jdbc:mysql://localhost:3306/";
    static final String BS = "skola";
    static final String UN = "magic";
    static final String PASS = "skola27";
    static Connection con = null;
    static ResultSet rsC = null;

// METOD unos koji se poziva iz unosIzvoda.java klase
    static int Id;
    
    public static void unos ( String primalac, String opis, String datum, double iznos ) {
        try {
            con = DriverManager.getConnection( URL + BS, UN, PASS);
            
            try (PreparedStatement psC = con.prepareStatement("SELECT Id FROM isplate")) {
                rsC = psC.executeQuery();
                while ( rsC.next() ) {
                    Id = rsC.getInt(1);
                }
                rsC.close();
            }
            Id++;
            
            
            String query = "INSERT INTO isplate ( Id, Datum, Primalac, OpisTransakcije, Iznos) VALUES(?,?,?,?,?)";
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setInt(1, Id);
                ps.setString(2, datum);
                ps.setString(3, primalac);
                ps.setString(4, opis);
                ps.setDouble(5, iznos);
                ps.executeUpdate();
            }
            con.close();
        
        } catch (SQLException ex) {
                Logger.getLogger(isplata.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
    
   