
package unos;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import pkg4u.forU;

/**
 *
 * @author Spremo
 */
public class unosUplate extends javax.swing.JFrame {

// promenljive vezane za MySQL
    static final String URL = "jdbc:mysql://localhost:3306/";
    static final String BS = "skola";
    static final String UN = "magic";
    static final String PASS = "skola27";
    static Connection con = null;
    static Statement stm = null;

// podaci preuzeti iz tabele izvodi    
    int brojIzvoda;
    double prethodnoStanje;
    String brojRacuna;
    
// podaci za ispis u prozoru
    static double  medjuUplata=0;
    static double medjuIsplata=0;
    static double medjuRacun=0;
// podaci preuzeti iz unosa    
    double iznos;
    String prezime;
    String ime;
    String datum;
// Promenljiva za pozivanje prozora greska    
    Greska greska = new Greska();
    
    
    int uplataBr, isplataBr;
       
    public unosUplate() {
        
        initComponents();
        
        ImageIcon img = new ImageIcon("src/image/4u_logo.jpg");
        this.setIconImage(img.getImage());
        
         try {
            con = DriverManager.getConnection( URL + BS, UN, PASS);
            stm = con.createStatement();
            
  // Rezultat izvrsenja preuzima rs          
            String query = "SELECT * FROM izvodi";
            
            try ( 
        // pripremljena izjava sa tekstom SELECT FROM
                PreparedStatement pst = con.prepareStatement(query); 
        // Rezultat izvrsenja preuzima rs
                    ResultSet rs = pst.executeQuery()) {
        // pregled svih izvoda i uzimanje vrednosti
                while ( rs.next() ) {
                    brojIzvoda++;
                    prethodnoStanje = rs.getDouble(7);
                }
            }
         }  catch (SQLException ex){
              ex.printStackTrace();
            }

// Ispis podataka o uplatama isplatama u prozoru
        
        labelPrethodnoStanje.setText("=" + prethodnoStanje + " ");
        labelUplate.setText("=" + medjuUplata + " ");
        
        if ( !(medjuUplata == 0) ) {
            uplataBr = uplataBr + 1;
            String uplataBrS = String.valueOf(uplataBr);
            jLabelUplataBr.setText(uplataBrS);
        }
            
        if ( !(medjuIsplata == 0) ) {
            isplataBr = isplataBr + 1;
            String isplataBrS = String.valueOf(isplataBr);
            jLabelIsplataBr.setText(isplataBrS);
        }
        
        labelIsplate.setText("=" + medjuIsplata + " ");
        medjuRacun = prethodnoStanje + medjuUplata - medjuIsplata;
        labelNovoStanje.setText("=" + medjuRacun + " ");
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldPrezime = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldIme = new javax.swing.JTextField();
        jButtonPotvrda = new javax.swing.JButton();
        jTextFieldIznos = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jXDatePicker = new org.jdesktop.swingx.JXDatePicker();
        labelNovoStanje = new javax.swing.JLabel();
        jLabelUplataBr = new javax.swing.JLabel();
        jLabelIsplataBr = new javax.swing.JLabel();
        prethodnoStanjeText = new javax.swing.JLabel();
        ukupnoUplataText = new javax.swing.JLabel();
        ukupnoIsplataText = new javax.swing.JLabel();
        novoStanjeText = new javax.swing.JLabel();
        labelPrethodnoStanje = new javax.swing.JLabel();
        labelUplate = new javax.swing.JLabel();
        labelIsplate = new javax.swing.JLabel();
        jButtonPotvrda1 = new javax.swing.JButton();
        jButtonOff = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("unos uplate");
        setLocation(new java.awt.Point(700, 340));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Unesite novu uplatu:");

        jPanel1.setBackground(new java.awt.Color(220, 220, 220));
        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Iznos");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Prezime");

        jTextFieldPrezime.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldPrezime.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldPrezime.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPrezime.setToolTipText("");
        jTextFieldPrezime.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTextFieldPrezime.setMaximumSize(new java.awt.Dimension(165, 25));
        jTextFieldPrezime.setMinimumSize(new java.awt.Dimension(165, 25));
        jTextFieldPrezime.setName(""); // NOI18N
        jTextFieldPrezime.setPreferredSize(new java.awt.Dimension(7, 25));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Ime");

        jTextFieldIme.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldIme.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldIme.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldIme.setToolTipText("");
        jTextFieldIme.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jButtonPotvrda.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButtonPotvrda.setText("potvrda");
        jButtonPotvrda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPotvrdaActionPerformed(evt);
            }
        });

        jTextFieldIznos.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldIznos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldIznos.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldIznos.setAction(jTextFieldPrezime.getAction());
        jTextFieldIznos.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTextFieldIznos.setMaximumSize(new java.awt.Dimension(165, 25));
        jTextFieldIznos.setMinimumSize(new java.awt.Dimension(165, 25));
        jTextFieldIznos.setName(""); // NOI18N
        jTextFieldIznos.setPreferredSize(new java.awt.Dimension(7, 25));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("datum");

        jXDatePicker.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jXDatePicker.setToolTipText("datum");

        labelNovoStanje.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelNovoStanje.setForeground(new java.awt.Color(153, 0, 51));
        labelNovoStanje.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelNovoStanje.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 3, true));

        jLabelUplataBr.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelUplataBr.setMaximumSize(new java.awt.Dimension(25, 25));
        jLabelUplataBr.setMinimumSize(new java.awt.Dimension(25, 25));
        jLabelUplataBr.setName(""); // NOI18N

        jLabelIsplataBr.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelIsplataBr.setMaximumSize(new java.awt.Dimension(25, 25));
        jLabelIsplataBr.setMinimumSize(new java.awt.Dimension(25, 25));
        jLabelIsplataBr.setName(""); // NOI18N

        prethodnoStanjeText.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        prethodnoStanjeText.setText("Prethodno stanje");

        ukupnoUplataText.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ukupnoUplataText.setText("Ukupno uplata");

        ukupnoIsplataText.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ukupnoIsplataText.setText("Ukupno isplata");

        novoStanjeText.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        novoStanjeText.setText("Novo stanje");

        labelPrethodnoStanje.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelPrethodnoStanje.setForeground(new java.awt.Color(153, 0, 51));
        labelPrethodnoStanje.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelPrethodnoStanje.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 3, true));

        labelUplate.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelUplate.setForeground(new java.awt.Color(153, 0, 51));
        labelUplate.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelUplate.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 3, true));

        labelIsplate.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelIsplate.setForeground(new java.awt.Color(153, 0, 51));
        labelIsplate.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelIsplate.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 3, true));

        jButtonPotvrda1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButtonPotvrda1.setText("prekid unosa");
        jButtonPotvrda1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPotvrda1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19)
                        .addComponent(jXDatePicker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jButtonPotvrda)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(19, 19, 19))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 1, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(18, 18, 18)))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTextFieldIznos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextFieldPrezime, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextFieldIme, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(0, 22, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(prethodnoStanjeText, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ukupnoUplataText, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ukupnoIsplataText, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(novoStanjeText, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabelUplataBr, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(labelUplate, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabelIsplataBr, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelNovoStanje, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(labelIsplate, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jButtonPotvrda1, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(labelPrethodnoStanje, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(20, 20, 20))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(labelUplate, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11)
                                .addComponent(jLabelIsplataBr, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(labelPrethodnoStanje, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(prethodnoStanjeText, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ukupnoUplataText, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabelUplataBr, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ukupnoIsplataText, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1))
                            .addComponent(labelIsplate, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelNovoStanje, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(novoStanjeText, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldIznos, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldPrezime, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldIme, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jXDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonPotvrda, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonPotvrda1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50))
        );

        jButtonOff.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButtonOff.setText("izlaz");
        jButtonOff.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jButtonOff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOffActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(80, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonOff)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addComponent(jButtonOff)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonOffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOffActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButtonOffActionPerformed
    private Double jTextFieldIznosActionPerformed(java.awt.event.ActionEvent evt) {
        String izn = jTextFieldIznos.getText();
        iznos = Double.parseDouble(izn);
        return iznos;
    }
    
    private String jTextFieldPrezimeActionPerformed(java.awt.event.ActionEvent evt) {                                                  
        prezime = jTextFieldPrezime.getText();
        return prezime;
    }  

    private String jTextFieldImeActionPerformed(java.awt.event.ActionEvent evt) {                                              
        ime = jTextFieldIme.getText();
        return ime;
    } 
    
    
    // Sakupljanje datuma iz tabele JXDatePicker
    // KONACNI IZGLED DATUMA 29-04-2020
   private String jXDatePickerActionPerformed(java.awt.event.ActionEvent evt) {                                              
       SimpleDateFormat formater = new SimpleDateFormat("dd-MM-YYYY");          // format za datum koji je potreban da bi se moglo operisati sa uzetim datumom krajnji izgled je 2020-04-29
       datum = formater.format(jXDatePicker.getDate());                  // dodela vrednosti String datumu u izabranom formatu sa pokupljenim datumom u tabeli jXDatePicker
       return datum;
       }                                             
   
    private void jButtonPotvrdaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPotvrdaActionPerformed
    
        iznos = jTextFieldIznosActionPerformed(evt);                            // uzima uneti iznos    
        prezime = jTextFieldPrezimeActionPerformed(evt);                        // uzima uneto prezime
        ime = jTextFieldImeActionPerformed(evt);                                // uzima uneto prezime
        datum = jXDatePickerActionPerformed(evt);                               // uzima uneti datum  
        String iznosString = String.valueOf(iznos);  
     
// CITANJE DATOTEKE MEDJURACUN ZA ISPIS U UNOSIZVODA 

        FileInputStream fisMR;
            try {
                fisMR = new FileInputStream("src/datoteke/medjuRacun.dat");
                Scanner sMR = new Scanner(fisMR);
                medjuUplata = sMR.nextDouble();
                medjuUplata = medjuUplata + iznos;
                medjuIsplata = sMR.nextDouble();
                medjuRacun = medjuUplata + medjuIsplata;
                labelUplate.setText("=" + medjuUplata + " ");
                labelIsplate.setText("=" + medjuIsplata + " ");
                labelNovoStanje.setText("=" + medjuRacun + " ");
                sMR.close();
                fisMR.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(unosUplate.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(unosUplate.class.getName()).log(Level.SEVERE, null, ex);
            }                
//  ZAVRSETAK RADA SA DATOTEKOM MEDJURACUN  
        try {
            // pozivanje klase unos sa unetim parametrima
            uplataUnos.unos( prezime, ime, datum, iznos, brojIzvoda );                  
        } catch (SQLException ex) {
            Logger.getLogger(unosUplate.class.getName()).log(Level.SEVERE, null, ex);
        }
        
// reset prozora nakon unosenja podataka, pozivanje novog unosa          
        dispose();                                                              // gasenje starog okvira
        unosUplate uU = new unosUplate();                                       // pravljenje novog okvira
        uU.setVisible(true);                                                    // postavljanje okvira da bude vidljiv
    }//GEN-LAST:event_jButtonPotvrdaActionPerformed

    private void jButtonPotvrda1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPotvrda1ActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonPotvrda1ActionPerformed
    
    public static void main(String args[]) {
        
        forU mainPage = new forU();
        mainPage.setVisible(true);
               
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new unosUplate().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonOff;
    private javax.swing.JButton jButtonPotvrda;
    private javax.swing.JButton jButtonPotvrda1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabelIsplataBr;
    private javax.swing.JLabel jLabelUplataBr;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextFieldIme;
    private javax.swing.JTextField jTextFieldIznos;
    private javax.swing.JTextField jTextFieldPrezime;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker;
    private javax.swing.JLabel labelIsplate;
    private javax.swing.JLabel labelNovoStanje;
    private javax.swing.JLabel labelPrethodnoStanje;
    private javax.swing.JLabel labelUplate;
    private javax.swing.JLabel novoStanjeText;
    private javax.swing.JLabel prethodnoStanjeText;
    private javax.swing.JLabel ukupnoIsplataText;
    private javax.swing.JLabel ukupnoUplataText;
    // End of variables declaration//GEN-END:variables

     
}

