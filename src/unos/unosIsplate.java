
package unos;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import pkg4u.forU;

public class unosIsplate extends javax.swing.JFrame {

        double iznos;
        int brojIzvoda;
        String opis;
        String primalac;
        String datum;
        Greska greska = new Greska();
        
    public unosIsplate() {
        initComponents();
        
        ImageIcon img = new ImageIcon("src/image/4u_logo.jpg");
        this.setIconImage(img.getImage());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldOpis = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldPrimalac = new javax.swing.JTextField();
        jButtonPotvrda = new javax.swing.JButton();
        jTextFieldIznos = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jXDatePicker = new org.jdesktop.swingx.JXDatePicker();
        jButtonOff = new javax.swing.JButton();
        jButtonOff1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("unos isplate");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Unesite novu isplatu:");

        jPanel1.setBackground(new java.awt.Color(220, 220, 220));
        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Iznos");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Opis");

        jTextFieldOpis.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldOpis.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldOpis.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldOpis.setToolTipText("");
        jTextFieldOpis.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Primalac");

        jTextFieldPrimalac.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldPrimalac.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldPrimalac.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPrimalac.setToolTipText("");
        jTextFieldPrimalac.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jButtonPotvrda.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButtonPotvrda.setText("potvrda");
        jButtonPotvrda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPotvrdaActionPerformed(evt);
            }
        });

        jTextFieldIznos.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldIznos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldIznos.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldIznos.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("datum");

        jButtonOff.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButtonOff.setText("prekid unosa");
        jButtonOff.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jButtonOff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOffActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jXDatePicker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextFieldPrimalac))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextFieldOpis))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextFieldIznos, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(10, 10, 10))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonOff, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButtonPotvrda, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldIznos, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextFieldOpis, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextFieldPrimalac, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jXDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addComponent(jButtonPotvrda, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonOff)
                .addContainerGap())
        );

        jButtonOff1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButtonOff1.setText("izlaz");
        jButtonOff1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jButtonOff1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOff1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButtonOff1))
                .addContainerGap(37, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 61, Short.MAX_VALUE)
                .addComponent(jButtonOff1)
                .addGap(25, 25, 25))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonOffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOffActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonOffActionPerformed

    private void jButtonPotvrdaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPotvrdaActionPerformed

        iznos = jTextFieldIznosActionPerformed(evt);                            // uzima uneti iznos
        opis = jTextFieldOpisActionPerformed(evt);                              // uzima uneti opis transakcije
        primalac = jTextFieldPrimalacActionPerformed(evt);                      // uzima podatke o primaocu
        datum = jXDatePickerActionPerformed(evt);                               // uzima uneti datum
        if ( opis.isEmpty() || opis.isBlank() ) { opis = "--"; }
        if ( primalac.isEmpty() || primalac.isBlank() ) { primalac = "--"; }
        isplata.unos( opis, primalac, datum, iznos );                       // pozivanje klase unos sa unetim parametrima
    // reset prozora nakon unosenja podataka, pozivanje novog unosa          
        dispose();                                                              // gasenje starog okvira
        unosIsplate uI = new unosIsplate();                                     // pravljenje novog okvira
        uI.setVisible(true);                                                    // postavljanje okvira da bude vidljiv
        
    }//GEN-LAST:event_jButtonPotvrdaActionPerformed

    private void jButtonOff1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOff1ActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonOff1ActionPerformed

    private Double jTextFieldIznosActionPerformed(java.awt.event.ActionEvent evt) {
        String izn = jTextFieldIznos.getText();
        iznos = Double.parseDouble(izn);
        return iznos;
    }
    
    private String jTextFieldOpisActionPerformed(java.awt.event.ActionEvent evt) {                                                  
        opis = jTextFieldOpis.getText();
        return opis;
    }
    
    private String jTextFieldPrimalacActionPerformed(java.awt.event.ActionEvent evt) {                                              
        primalac = jTextFieldPrimalac.getText();
        return primalac;
    } 
    
    // Sakupljanje datuma iz tabele JXDatePicker
    // KONACNI IZGLED DATUMA 2020-04-29
    private String jXDatePickerActionPerformed(java.awt.event.ActionEvent evt) {                                              
       SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");          // format za datum koji je potreban da bi se moglo operisati sa uzetim datumom krajnji izgled je 2020-04-29
       String datum = formater.format(jXDatePicker.getDate());                  // dodela vrednosti String datumu u izabranom formatu sa pokupljenim datumom u tabeli jXDatePicker
       return datum;
       }                                             
   
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        forU mainPage = new forU();
        mainPage.setVisible(true);
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new unosIsplate().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonOff;
    private javax.swing.JButton jButtonOff1;
    private javax.swing.JButton jButtonPotvrda;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextFieldIznos;
    private javax.swing.JTextField jTextFieldOpis;
    private javax.swing.JTextField jTextFieldPrimalac;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker;
    // End of variables declaration//GEN-END:variables
}
