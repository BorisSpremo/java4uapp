
package unos;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;


public class unosIzvoda extends javax.swing.JFrame {

    
    
    String datum;
    int brojIzvoda, brojIzvodaStari;
    String brojIzvodaString;
    double iznos;
    static double ukupnoUplata=0, ukupnoIsplata=0;
    static double novoStanje, prethodnoStanje;
    String prethodnoStanjeString, banka, racunRSD;
    Greska greska = new Greska();
    boolean plus;
    
    public unosIzvoda() {
        initComponents();
        
        ImageIcon img = new ImageIcon("src/image/4u_logo.jpg");
        this.setIconImage(img.getImage());
        
       
        FileInputStream fis;
        Scanner s;
        try {
            fis = new FileInputStream("src/datoteke/stanjeRacuna.txt");
            s = new Scanner(fis);
            
            prethodnoStanje =  s.nextDouble();
            s.nextLine();
            banka = s.nextLine();
            racunRSD = s.nextLine();;
            brojIzvodaStari = s.nextInt();
            s.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Nije nadjena datoteka stanjeRacuna.dat. ");
        } catch (IOException ex) {
            System.out.println("IO problem sa datotekom stanjeRacuna.dat. ");
        }
                
// ISPIS U POLJIMA TEKSTA IZ MEMORIJE
        prethodnoStanjeString = String.valueOf(prethodnoStanje);
        bankaText.setText(banka);
        brojRacunaRSD.setText(racunRSD);
        brojIzvoda = brojIzvodaStari + 1;
        brojIzvodaString = String.valueOf( brojIzvoda );
        jTextFieldBrojIzvoda.setText(brojIzvodaString);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        ZaglavljeText = new javax.swing.JLabel();
        jButtonOff = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        bankaText = new javax.swing.JLabel();
        brojIzvodaText = new javax.swing.JLabel();
        jButtonPotvrda = new javax.swing.JButton();
        jTextFieldBrojIzvoda = new javax.swing.JTextField();
        brojRacunText = new javax.swing.JLabel();
        brojRacunaRSD = new javax.swing.JLabel();
        jButtonZavrsetak = new javax.swing.JButton();
        jRadioButtonUplata = new javax.swing.JRadioButton();
        jRadioButtonIsplata = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("frame");

        ZaglavljeText.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ZaglavljeText.setText("Unesite novi izvod:");

        jButtonOff.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButtonOff.setText("izlaz");
        jButtonOff.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jButtonOff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOffActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(220, 220, 220));
        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        bankaText.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        brojIzvodaText.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        brojIzvodaText.setText("Broj izvoda");

        jButtonPotvrda.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButtonPotvrda.setText("potvrda");
        jButtonPotvrda.setPreferredSize(new java.awt.Dimension(90, 25));
        jButtonPotvrda.setRequestFocusEnabled(false);
        jButtonPotvrda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPotvrdaActionPerformed(evt);
            }
        });

        jTextFieldBrojIzvoda.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldBrojIzvoda.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldBrojIzvoda.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldBrojIzvoda.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        brojRacunText.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        brojRacunText.setText("Broj računa");

        brojRacunaRSD.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jButtonZavrsetak.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButtonZavrsetak.setText("snjimanje i izlaz");
        jButtonZavrsetak.setPreferredSize(new java.awt.Dimension(90, 25));
        jButtonZavrsetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonZavrsetakActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButtonUplata);
        jRadioButtonUplata.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jRadioButtonUplata.setText("Uplata");
        jRadioButtonUplata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonUplataActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButtonIsplata);
        jRadioButtonIsplata.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jRadioButtonIsplata.setText("Isplata");
        jRadioButtonIsplata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonIsplataActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(brojIzvodaText, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(280, 280, 280))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButtonZavrsetak, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldBrojIzvoda, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(brojRacunText)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(brojRacunaRSD, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(60, 60, 60))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(bankaText, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addComponent(jRadioButtonUplata)
                        .addGap(60, 60, 60)
                        .addComponent(jRadioButtonIsplata)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(126, 126, 126)
                .addComponent(jButtonPotvrda, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bankaText, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(brojRacunaRSD, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(brojRacunText, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(brojIzvodaText, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldBrojIzvoda, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButtonUplata)
                    .addComponent(jRadioButtonIsplata))
                .addGap(18, 18, 18)
                .addComponent(jButtonPotvrda, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65)
                .addComponent(jButtonZavrsetak, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonOff))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(56, 56, 56)
                                .addComponent(ZaglavljeText, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(44, 44, 44)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 42, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(ZaglavljeText, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addComponent(jButtonOff)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    
          
    private void jButtonOffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOffActionPerformed
        if ( ukupnoUplata==0 && ukupnoIsplata==0 ) {
            System.exit(0);
        } else {
            unosIzvoda unos = new unosIzvoda();
            unos.setVisible(true);
          }
    }//GEN-LAST:event_jButtonOffActionPerformed

    private void jButtonPotvrdaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPotvrdaActionPerformed

        brojIzvoda = jTextFieldBrojIzvodaActionPerformed(evt);
        if ( jRadioButtonUplata.isSelected() ) {
            unosUplate unos = new unosUplate();
            unos.setVisible(true);
        } else if ( jRadioButtonIsplata.isSelected() ) {
                    unosIsplate unosI = new unosIsplate();
                    unosI.setVisible(true);
        }
        
    }//GEN-LAST:event_jButtonPotvrdaActionPerformed

    private void jButtonZavrsetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonZavrsetakActionPerformed
        // snimanje i izlaz
        dispose();
    }//GEN-LAST:event_jButtonZavrsetakActionPerformed

    private void jRadioButtonUplataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonUplataActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButtonUplataActionPerformed

    private void jRadioButtonIsplataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonIsplataActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButtonIsplataActionPerformed

    private int jTextFieldBrojIzvodaActionPerformed(java.awt.event.ActionEvent evt) {                                                          
        String brojIzvodaT= jTextFieldBrojIzvoda.getText();
        brojIzvoda = Integer.valueOf(brojIzvodaT);
        return brojIzvoda;
        
    } 
     
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(unosIzvoda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(unosIzvoda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(unosIzvoda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(unosIzvoda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new unosIzvoda().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ZaglavljeText;
    private javax.swing.JLabel bankaText;
    private javax.swing.JLabel brojIzvodaText;
    private javax.swing.JLabel brojRacunText;
    private javax.swing.JLabel brojRacunaRSD;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButtonOff;
    private javax.swing.JButton jButtonPotvrda;
    private javax.swing.JButton jButtonZavrsetak;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButtonIsplata;
    private javax.swing.JRadioButton jRadioButtonUplata;
    private javax.swing.JTextField jTextFieldBrojIzvoda;
    // End of variables declaration//GEN-END:variables

}
