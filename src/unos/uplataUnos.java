 
package unos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import java.sql.*;
import java.sql.DriverManager;
import java.sql.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import pkg4u.uceniciObjekat;

/**
 *
 * @author Spremo
 */
public class uplataUnos {
    
// promenljive vezane za MySQL
    static final String URL = "jdbc:mysql://localhost:3306/";
    static final String BD = "skola";
    static final String UN = "magic";
    static final String PASS = "skola27";
    static Connection con = null;    

// podaci o polazniku    
    static int IdPolaznika;
    static String adresa;
    static String mesto;
    static int brojRate = 0;
    static int brojRacuna;
    
    static int rb;
    static int rbKontrolni=0;
    static int brojUplata, brojUcenikaSaUplatom=0;
    static String upis;
    //static String ulazIzlaz;
    
    static int Id;
    
    
    static double prethodnoStanje, novoStanje;
    static double medjuUplata=0, medjuIsplata=0, medjuRacun=0;
    static double zbir;
    static String banka;
    static String racunRSD;
    static int brojIzvoda=0;
    static int brojIzvodaStari;
    
    
    public static void unos ( String prezime, String ime, String datum, double iznos, int brojIzvoda ) throws SQLException 

    {
             
        try {
// Citanje kompletnih podataka iz tabele polaznik         
            con = (Connection) DriverManager.getConnection( URL + BD, UN, PASS );
        
            String query = "SELECT * FROM polaznik";
            ResultSet rsP;
            
            try (PreparedStatement psP = con.prepareStatement(query)) {
                rsP = psP.executeQuery();
                while (rsP.next()) {
                    String imeTest = rsP.getString(2);
                    String prezimeTest = rsP.getString(3);
                    if ( prezimeTest.equals(prezime) && imeTest.equals(ime)) {
                        IdPolaznika = rsP.getInt(1);
                        adresa = rsP.getString(9);
                        mesto = rsP.getString(10) + " " + rsP.getString(11);
                        break;
                    }
                }
                rsP.close();
            }
            
// TABELA RACUN uzimanje podataka za broj Racuna kao i broj Rate

            query = "SELECT * FROM racun";
            ResultSet rsR;
            
            try (PreparedStatement psR = con.prepareStatement(query)) {
                rsR = psR.executeQuery();
                while (rsR.next()) {
                    brojRacuna = rsR.getInt(1);
                    String imeTest = rsR.getString(2);
                    String prezimeTest = rsR.getString(3);
                    int brojRateTest = rsR.getInt(8);
                    if ( prezimeTest.equals(prezime) && imeTest.equals(ime)) {
                        brojRate = brojRateTest;
                    }
                }
                rsR.close();
            }
// uvecavanje broja Racuna i broja Rate za upis            
            brojRacuna++;
            brojRate++;
            
// TABELA RACUN unos kompletnih podataka, tj, unos racuna
            Statement st = con.createStatement();
            String query1 = brojRacuna + ", '" + ime + "', '" + prezime + "', '" + adresa + "', '" + mesto + "', '" + datum + "', " + iznos + ", " + brojRate;
            query = "INSERT INTO racun(BrojRacuna, Ime, Prezime, Adresa, Mesto, Datum, Iznos, brojRate ) VALUES(" + query1 + ")";
            st.executeUpdate(query);
            
        } catch (SQLException ex) {
            Logger.getLogger(uplataUnos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        pravljenjeRacuna.racun();
        
// TABELA uplate unos kompletnih podataka        
        try {
            String query1 = "SELECT * FROM uplate";
            PreparedStatement psR = con.prepareStatement(query1);
            ResultSet rs = psR.executeQuery();
            while ( rs.next() ) {
                Id = rs.getInt(1);
            }
            Id++;
            
            String uplatilac = ime + " " + prezime;
            String query = "INSERT INTO uplate( Id, Datum, Uplatilac , Iznos ) VALUES(?,?,?,?)"; 
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setInt(1, Id);
                ps.setString(2,datum);
                ps.setString(3, uplatilac);
                ps.setDouble(4, iznos);
                ps.executeUpdate();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(uplataUnos.class.getName()).log(Level.SEVERE, null, ex);
        }
        con.close();
    }
}