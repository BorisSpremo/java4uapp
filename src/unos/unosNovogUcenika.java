/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unos;


import java.io.FileNotFoundException;
import javax.swing.ImageIcon;
import pkg4u.forU;
import pkg4u.uceniciObjekat;

/**
 *
 * @author Spremo
 */
public class unosNovogUcenika extends javax.swing.JFrame {

    /**
     * Creates new form unosNovogUcenika
     */
    String ime, prezime, grupa, mail;
    int godiste, dete;
    double rataIznos;
    Greska greska = new Greska();
    
    public unosNovogUcenika() {
        initComponents();
        
        
        ImageIcon img = new ImageIcon("src/image/4u_logo.jpg");                 // ucitavanje fotografije iz baze
        this.setIconImage(img.getImage());                                      // dodeljuje fotografiju u zaglavlje prozora
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jButtonOff = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldPrezime = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldGrupa = new javax.swing.JTextField();
        jButtonPotvrda = new javax.swing.JButton();
        jTextFieldIme = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldRata = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldGodiste = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jRadioButton3 = new javax.swing.JRadioButton();
        jTextFieldEmail = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jButtonPotvrda1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("unos novog učenika");
        setLocation(new java.awt.Point(740, 340));
        setPreferredSize(new java.awt.Dimension(400, 550));

        jButtonOff.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButtonOff.setText("izlaz");
        jButtonOff.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jButtonOff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOffActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Podaci za unos novog učenika");

        jPanel1.setBackground(new java.awt.Color(220, 220, 220));
        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setPreferredSize(new java.awt.Dimension(300, 330));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Grupa");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Prezime");

        jTextFieldPrezime.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldPrezime.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldPrezime.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldPrezime.setToolTipText("");
        jTextFieldPrezime.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTextFieldPrezime.setMinimumSize(new java.awt.Dimension(7, 25));
        jTextFieldPrezime.setPreferredSize(new java.awt.Dimension(7, 25));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Ime");

        jTextFieldGrupa.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldGrupa.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldGrupa.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldGrupa.setToolTipText("");
        jTextFieldGrupa.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTextFieldGrupa.setMinimumSize(new java.awt.Dimension(7, 25));
        jTextFieldGrupa.setPreferredSize(new java.awt.Dimension(7, 25));

        jButtonPotvrda.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButtonPotvrda.setText("potvrda");
        jButtonPotvrda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPotvrdaActionPerformed(evt);
            }
        });

        jTextFieldIme.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldIme.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldIme.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldIme.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTextFieldIme.setMaximumSize(new java.awt.Dimension(7, 25));
        jTextFieldIme.setMinimumSize(new java.awt.Dimension(7, 25));
        jTextFieldIme.setPreferredSize(new java.awt.Dimension(7, 25));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Rata");

        jTextFieldRata.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldRata.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldRata.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldRata.setToolTipText("");
        jTextFieldRata.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTextFieldRata.setMaximumSize(new java.awt.Dimension(7, 25));
        jTextFieldRata.setMinimumSize(new java.awt.Dimension(7, 25));
        jTextFieldRata.setPreferredSize(new java.awt.Dimension(7, 25));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Upisan u školu kao");

        jTextFieldGodiste.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldGodiste.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldGodiste.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldGodiste.setToolTipText("");
        jTextFieldGodiste.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTextFieldGodiste.setMaximumSize(new java.awt.Dimension(7, 25));
        jTextFieldGodiste.setMinimumSize(new java.awt.Dimension(7, 25));
        jTextFieldGodiste.setPreferredSize(new java.awt.Dimension(7, 25));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Godište");

        jRadioButton1.setBackground(new java.awt.Color(220, 220, 220));
        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jRadioButton1.setSelected(true);
        jRadioButton1.setText("I dete");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        jRadioButton2.setBackground(new java.awt.Color(220, 220, 220));
        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jRadioButton2.setText("II dete");

        jRadioButton3.setBackground(new java.awt.Color(220, 220, 220));
        buttonGroup1.add(jRadioButton3);
        jRadioButton3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jRadioButton3.setText("III dete");

        jTextFieldEmail.setBackground(new java.awt.Color(204, 204, 204));
        jTextFieldEmail.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextFieldEmail.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldEmail.setToolTipText("");
        jTextFieldEmail.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTextFieldEmail.setMaximumSize(new java.awt.Dimension(7, 25));
        jTextFieldEmail.setMinimumSize(new java.awt.Dimension(7, 25));
        jTextFieldEmail.setPreferredSize(new java.awt.Dimension(7, 25));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("e-mail");

        jButtonPotvrda1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButtonPotvrda1.setText("prekid unosa");
        jButtonPotvrda1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPotvrda1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jRadioButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jRadioButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jRadioButton3)
                        .addGap(30, 30, 30))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextFieldGodiste, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jTextFieldIme, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jTextFieldPrezime, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextFieldGrupa, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jButtonPotvrda)
                                .addComponent(jButtonPotvrda1)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextFieldEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldRata, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addContainerGap(29, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextFieldIme, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldPrezime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldGrupa, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldGodiste, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2)
                    .addComponent(jRadioButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldRata, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addComponent(jButtonPotvrda)
                .addGap(18, 18, 18)
                .addComponent(jButtonPotvrda1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(145, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonOff)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 66, Short.MAX_VALUE)
                .addComponent(jButtonOff)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonOffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOffActionPerformed
        System.exit(0);                                                         // klik na dugme off zatvara program
    }//GEN-LAST:event_jButtonOffActionPerformed

    // dugme za potvrdu unosa podataka
    private void jButtonPotvrdaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPotvrdaActionPerformed
        
        ime = jTextFieldIme.getText();                                          // dodeljuje tekst iz polja Stringu ime 
        prezime = jTextFieldPrezime.getText();                                  // dodeljuje tekst iz polja Stringu prezime 
        grupa = jTextFieldGrupa.getText();                                      // dodeljuje tekst iz polja Stringu grupa 
        mail = jTextFieldEmail.getText(); 
        String godisteT = jTextFieldGodiste.getText();                          // dodeljuje tekst iz polja Stringu godisteT 
        try {
            godiste = Integer.parseInt(godisteT); }                             // uz try pokusava da iz stringa dobijemo broj int koji dodeljujemo intu godiste
        catch(NumberFormatException ex) {
            greska.setVisible(true);                                            // ako uneta vrednost nije broj izbacuje gresku
        }
        String rataIznosT = jTextFieldRata.getText();                           // uz try pokusava da iz stringa dobijemo broj double koji dodeljujemo doublu rataIznos
        try {
            rataIznos = Double.valueOf(rataIznosT); }
        catch(NumberFormatException ex) {
            greska.setVisible(true);
        }
        
        
        if (jRadioButton1.isSelected()){                                        // radio dugmad za redni broj upisanog deteta  1 ili 2 ili 3
            dete = 1;
        } else if (jRadioButton2.isSelected()){
            dete = 2;
        } else if (jRadioButton3.isSelected()){
            dete = 3;
        }
        
        uceniciObjekat.ucenici( ime, prezime, grupa, godiste, dete, rataIznos, mail ); // poziva classu uceniciObjekat i metod ucenici koji ubacuje podatke u datoteku
        prekidIReset();                                                     // poziv metoda prekidReset
    
    }//GEN-LAST:event_jButtonPotvrdaActionPerformed
    // metod koji resetuje jFrame unosNovogUcenika, za novi unos
    public void prekidIReset() {                
        dispose();                                                              // gasenje starog okvira
        unosNovogUcenika uNU = new unosNovogUcenika();                          // pravljenje novog okvira
        uNU.setVisible(true);                                                   // postavljanje okvira da bude vidljiv
    }
    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jButtonPotvrda1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPotvrda1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonPotvrda1ActionPerformed
    
    private String jTextFieldEmailActionPerformed(java.awt.event.ActionEvent evt) {                                                
        String mail = jTextFieldEmail.getText();                                // mail1 dobija vrednost koja je procitana iz polja Ime
        return mail;                                                            // metod vraca dobijeni string po pozivu
        
    } 
    private String jTextFieldImeActionPerformed(java.awt.event.ActionEvent evt) {                                              
        String ime1 = jTextFieldIme.getText();                                  // ime1 dobija vrednost koja je procitana iz polja Ime
        return ime1;                                                            // metod vraca dobijeni string po pozivu
    } 
    
    private String jTextFieldPrezimeActionPerformed(java.awt.event.ActionEvent evt) {                                              
        String prezime1 = jTextFieldIme.getText();
        return prezime1;
    } 
    
    private String jTextFieldGrupaActionPerformed(java.awt.event.ActionEvent evt) {                                              
        String grupa1 = jTextFieldIme.getText();
        return grupa1;
    }
    
    private String jTextFieldGodisteActionPerformed(java.awt.event.ActionEvent evt) {                                              
        String godiste1 = jTextFieldIme.getText();
        return godiste1;
    }
    
    private double jTextFieldRataActionPerformed(java.awt.event.ActionEvent evt) {                                              
        String rata = jTextFieldIme.getText();
        double rataIznos1 = Double.valueOf(rata);
        return rataIznos1;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        forU mainPage = new forU();
        mainPage.setVisible(true);
        
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(unosNovogUcenika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(unosNovogUcenika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(unosNovogUcenika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(unosNovogUcenika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new unosNovogUcenika().setVisible(true);
            }
        });
    
        
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButtonOff;
    private javax.swing.JButton jButtonPotvrda;
    private javax.swing.JButton jButtonPotvrda1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JTextField jTextFieldEmail;
    private javax.swing.JTextField jTextFieldGodiste;
    private javax.swing.JTextField jTextFieldGrupa;
    private javax.swing.JTextField jTextFieldIme;
    private javax.swing.JTextField jTextFieldPrezime;
    private javax.swing.JTextField jTextFieldRata;
    // End of variables declaration//GEN-END:variables
}
