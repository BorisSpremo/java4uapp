
package pkg4u;

import java.sql.*;
import java.sql.Connection;
import java.sql.Statement;
import unos.Greska;

/**
 *
 * @author Spremo
 */
public class uceniciObjekat {
// promenljive vezane za MySQL
    static final String URL = "jdbc:mysql://localhost:3306/";
    static final String BS = "skola";
    static final String UN = "magic";
    static final String PASS = "skola27";
    static Connection con = null;
    static Statement stm = null;
    
// Promenljive za rad sa programom            
    String ime, prezime, grupa;
    int godiste;
    private static int rb, dete;
    double rataIznos;
    static int test;
    static Greska greska = new Greska();
    
public static void ucenici( String ime, String prezime, String grupa, int godiste, int dete, double rataIznos, String mail ) {
        
        try {
            con = DriverManager.getConnection( URL + BS, UN, PASS);
            stm = con.createStatement();
            
            String sql = "INSERT INTO polaznik (Ime, Prezime, Grupa, Godiste, DetePoRedu, Rata, email) VALUES(?,?,?,?,?,?,?)";
            
            try ( PreparedStatement ps = con.prepareStatement(sql) ) {
            
                ps.setString(1,ime);
                ps.setString(2, prezime);
                ps.setString(3,grupa);
                ps.setInt(4,godiste);
                ps.setInt(5,dete);
                ps.setDouble(6,rataIznos);
                ps.setString(7,mail);
                
                ps.executeUpdate();
                
                ps.close();
                stm.close();
                con.close();
                
            }   catch (SQLException ex){
                    ex.printStackTrace();
                }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
}
public static void greska(){
    greska.setVisible(true);                                    // poziv za jFrame greska
}
}
