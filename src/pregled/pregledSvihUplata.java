package pregled;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.ScrollPane;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 *
 * @author Spremo
 */
public class pregledSvihUplata extends JPanel {

    static int brojHF1, brojHF2, 
               brojKB1, brojKB2, brojKB3, brojKB4, brojKB5, brojKB6, 
               brojKEY, brojPET, brojFCE, brojPRO, brojB2, brojNE;
     static String tekstDuzi;
    
    static String tekst = null;
    
    public static void main(String[] args) {
        
    }
// TABELA TABELA        
public static void tabela(){
    
        int redniBroj=0;
        
// Odredjivanje broja clanova koji su upisani u datoteku Ucenici.txt
        FileInputStream fis;
        try {
            fis = new FileInputStream("src/datoteke/Ucenici.txt");
            Scanner s = new Scanner(fis);
            while (s.hasNext()) {
                 redniBroj = redniBroj + 1; 
                 s.nextLine();           }
            s.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Ne postoji datoteka Ucenici.txt. ");
        } catch (IOException ex) {
            System.out.println("IO problem sa datotekom Ucenici.txt. ");
        }
// Kraj iscitavanja datoteka
        
        String prezime[] = new String[redniBroj];       // prezime ucenika
        String ime[] = new String[redniBroj];           // ime icenika
        String kurs[] = new String[redniBroj];          // kurs kojem ucenik pripada
        int redniB[] = new int[redniBroj];              // redni broj ucenika pri upisu u bazu
        int red[] = new int[redniBroj];                 // broj za kurs koji se dobija metodom odredjivanjeKursa()
        double rata[] = new double[redniBroj];          // rata predvidjena za uplate
        
       try {
// Citanje podataka iz datoteke ucenici.txt i pohranjivanje u nizove
            fis = new FileInputStream("src/datoteke/Ucenici.txt");
            Scanner s = new Scanner(fis);
            for ( int i=0; i<redniBroj; i++ ) {
                redniB[i] = s.nextInt();
                prezime[i] = s.next();
                ime[i] = s.next();
                kurs[i] = s.next();
                String kursK = kurs[i];
                red[i] = odredjivanjeKursa (kursK);
                s.nextInt();
                rata[i] = s.nextDouble();
            } 
        s.close();
        fis.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Ne postoji datoteka Ucenici.txt. ");
        } catch (IOException ex) {
            System.out.println("IO problem sa datotekom Ucenici.txt. ");
        }     
// Zavrsetak citanje podataka iz datoteke ucenici.txt 
        
// Pravljenje liste po kursevima, zamena mesta ucenika sa podacima po kursevima od slabijih ka jacim
        for ( int k=0; k<redniBroj; k++ ) {
            for ( int j=0; j<redniBroj; j++ ) {
                if ( red[j] > red[k] ) {    
                    int rb = redniB[j];         
                    redniB[j] = redniB[k];      
                    redniB[k] = rb;             
                    String pr = prezime[j];     
                    prezime[j] = prezime[k];
                    prezime[k] = pr;
                    String im = ime[j];
                    ime[j] = ime[k];
                    ime[k] = im;
                    int redP = red[j];
                    red[j] = red[k];
                    red[k] = redP;
                    String ku = kurs[j];
                    kurs[j] = kurs[k];
                    kurs[k] = ku;
                    double ra = rata[j];
                    rata[j] = rata[k];
                    rata[k] = ra;
                }
            }
        }
// Lista je pripremljena spremna za 
        
// Iscitavanje uplata iz datoteke uplate.txt
        int brojRata[] = new int[redniBroj];
        String rataUp[][] = new String[10][redniBroj];                          // [10] broj rate [redniBroj] broj ucenika ==  iznos rate
        String datumUp[][] = new String[10][redniBroj];
        
        try {
                FileInputStream fileIS = new FileInputStream("src/datoteke/uplate.txt");
                Scanner s = new Scanner(fileIS);
                
                for ( int b=0; b<redniBroj; b++ ) {
                    s.nextInt();
                    brojRata[b] = s.nextInt();
                    for ( int a=0; a<10; a++) {
                        s.nextInt();
                        if ( a < brojRata[b] ) {
                            datumUp[a][b] = s.next();
                            rataUp[a][b] = s.next();
                        } else {
                            datumUp[a][b] = null;
                            rataUp[a][b] = null;
                            s.next();
                            s.next();
                        }
                    }
                }
                s.close();
                fileIS.close();
            } catch (FileNotFoundException ex) {
                System.out.println("Nije nadjena datoteka uplate.txt");
            } catch (IOException ex) {
                System.out.println("Problem sa citanjem podataka iz datoteke uplate.txt");
           }
        
// Sortiranje liste liste po kursevima
        String rataZapis[][] = new String[10][redniBroj];            // novi niz za rate koji ce sadrzati sve uplate po redosledu izlaska u listi, poredjani po kursevima
        String datumZapis[][] = new String[10][redniBroj];           // -||- za datume uplata
        for ( int k=0; k<redniBroj; k++ ) {                        
            for ( int j=0; j<10; j++ ) {                           
                int pozicija1 = redniB[k]-1;                        // redniB[k] je redni broj ucenika kako je u bazi primera redniB[1] jednak je 3 poziciji u unosu
                if ( k < redniBroj-1 ){
                    rataZapis[j][k] = rataUp[j][pozicija1];         //rataZapis je redosled u zapisu a rataUp je redosled po upisu ucenika
                    String datum = datumUp[j][pozicija1];            
                    datumZapis[j][k] = datumUp[j][pozicija1];              
                }
            }
        }
// kraj sortiranje

        int pozicijaX = 30, 
            pozicijaY = 60;
            
        JFrame f = new JFrame();
        JPanel panel = new JPanel(null);
        
        panel.setPreferredSize(new Dimension(1400, 1600));
        
        
        for ( int i=0; i<redniBroj; i++ ){
            Color bojaPolja = boja(kurs[i]);
            Color bojaPoljaRata = new Color( 202,204,206 );
            Color bojaPoljaDatum = new Color ( 180,180,180);
            
            JLabel labelKurs, labelUcenik, labelCena,
               labelRata1, labelRata2, labelRata3, labelRata4, labelRata5, 
               labelRata6, labelRata7, labelRata8, labelRata9, labelRata10,
               labelDatum1, labelDatum2, labelDatum3, labelDatum4, labelDatum5, 
               labelDatum6, labelDatum7, labelDatum8, labelDatum9, labelDatum10;
            
            labelKurs = new JLabel();  
            labelUcenik = new JLabel();        
            labelCena = new JLabel();        
        
            labelRata1 = new JLabel();        labelDatum1 = new JLabel();        
            labelRata2 = new JLabel();        labelDatum2 = new JLabel();        
            labelRata3 = new JLabel();        labelDatum3 = new JLabel();
            labelRata4 = new JLabel();        labelDatum4 = new JLabel();
            labelRata5 = new JLabel();        labelDatum5 = new JLabel();
            labelRata6 = new JLabel();        labelDatum6 = new JLabel();
            labelRata7 = new JLabel();        labelDatum7 = new JLabel();
            labelRata8 = new JLabel();        labelDatum8 = new JLabel();
            labelRata9 = new JLabel();        labelDatum9 = new JLabel();
            labelRata10 = new JLabel();       labelDatum10 = new JLabel();
        
            panel.setLayout(null);
            panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
            // labelKurs 
            labelOsnovni(labelKurs, 70, 50, 30, pozicijaX, kurs[i] , bojaPolja );
            labelOsnovni(labelUcenik, 150, 50, 105, pozicijaX, ime[i] + " " + prezime[i], bojaPolja );
            String rataP = String.valueOf(rata[i]);
            labelOsnovni(labelCena, 70, 50, 260, pozicijaX, rataP, bojaPolja );
        
            panel.add(labelKurs);
            panel.add(labelUcenik);
            panel.add(labelCena);
            
            String rataIzlaz[] = new String [10];
            String datumIzlaz[] = new String [10];
            for ( int j=0; j<10; j++ ) {
                rataIzlaz[j] = rataZapis[j][i];
                datumIzlaz[j] = datumZapis[j][i];
                System.out.println("rata " + rataIzlaz[j]);
                System.out.println("datum " + datumIzlaz[j]);
            }
            
            
            labelOsnovni( labelRata1,  100, 25,  335, pozicijaX, rataIzlaz[0], bojaPoljaRata );      labelOsnovni( labelDatum1,  100, 20,  335, pozicijaY, datumIzlaz[0], bojaPoljaDatum );
            labelOsnovni( labelRata2,  100, 25,  440, pozicijaX, rataIzlaz[1], bojaPoljaRata );      labelOsnovni( labelDatum2,  100, 20,  440, pozicijaY, datumIzlaz[1], bojaPoljaDatum );
            labelOsnovni( labelRata3,  100, 25,  545, pozicijaX, rataIzlaz[2], bojaPoljaRata );      labelOsnovni( labelDatum3,  100, 20,  545, pozicijaY, datumIzlaz[2], bojaPoljaDatum );
            labelOsnovni( labelRata4,  100, 25,  650, pozicijaX, rataIzlaz[3], bojaPoljaRata );      labelOsnovni( labelDatum4,  100, 20,  650, pozicijaY, datumIzlaz[3], bojaPoljaDatum );
            labelOsnovni( labelRata5,  100, 25,  755, pozicijaX, rataIzlaz[4], bojaPoljaRata );      labelOsnovni( labelDatum5,  100, 20,  755, pozicijaY, datumIzlaz[4], bojaPoljaDatum );
            labelOsnovni( labelRata6,  100, 25,  860, pozicijaX, rataIzlaz[5], bojaPoljaRata );      labelOsnovni( labelDatum6,  100, 20,  860, pozicijaY, datumIzlaz[5], bojaPoljaDatum );
            labelOsnovni( labelRata7,  100, 25,  965, pozicijaX, rataIzlaz[6], bojaPoljaRata );      labelOsnovni( labelDatum7,  100, 20,  965, pozicijaY, datumIzlaz[6], bojaPoljaDatum );
            labelOsnovni( labelRata8,  100, 25, 1070, pozicijaX, rataIzlaz[7], bojaPoljaRata );      labelOsnovni( labelDatum8,  100, 20, 1070, pozicijaY, datumIzlaz[7], bojaPoljaDatum );
            labelOsnovni( labelRata9,  100, 25, 1175, pozicijaX, rataIzlaz[8], bojaPoljaRata );      labelOsnovni( labelDatum9,  100, 20, 1175, pozicijaY, datumIzlaz[8], bojaPoljaDatum );
            labelOsnovni( labelRata10, 100, 25, 1280, pozicijaX, rataIzlaz[9], bojaPoljaRata );      labelOsnovni( labelDatum10, 100, 20, 1280, pozicijaY, datumIzlaz[9], bojaPoljaDatum );
        
            panel.add(labelRata1);        panel.add(labelDatum1);
            panel.add(labelRata2);        panel.add(labelDatum2);
            panel.add(labelRata3);        panel.add(labelDatum3);
            panel.add(labelRata4);        panel.add(labelDatum4);
            panel.add(labelRata5);        panel.add(labelDatum5);
            panel.add(labelRata6);        panel.add(labelDatum6);
            panel.add(labelRata7);        panel.add(labelDatum7);
            panel.add(labelRata8);        panel.add(labelDatum8);
            panel.add(labelRata9);        panel.add(labelDatum9);
            panel.add(labelRata10);       panel.add(labelDatum10);
                      
            if ( i < redniBroj-1 ) {
                if ( !kurs[i].equals(kurs[i+1])) {
                    pozicijaX = pozicijaX + 20;
                }
            }
            
            pozicijaX = pozicijaX + 60;
            pozicijaY = pozicijaX + 30;
        
        }    
        
        JScrollPane scrollPanel = new JScrollPane(panel);
        scrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        
        scrollPanel.setBounds( 10, 10, 1420, 590);
        
        JPanel contentPane = new JPanel(null);
        contentPane.setPreferredSize(new Dimension(1440,610));
        contentPane.add(scrollPanel);
        
        f.setContentPane(contentPane);
        f.setBounds(150, 100, 1460, 630);
        f.pack();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
        
}
      
// KRAJ MAIN METODA

//  METOD LABELOSNOVNI kreiranje izlaznog labela       // ulazne vrednosti su label , dimenzija prozora X i Y, pozicija X i Y , ispis teksta i boja polja
    private static void labelOsnovni( JLabel label, int dimenzijaX , int dimenzijaY, int pozicijaX, int pozicijaY, String text, Color boja ) {
        
        
        Dimension size = label.getPreferredSize();
        label.setBounds( pozicijaX, pozicijaY, dimenzijaX, dimenzijaY );        // uvezene dimenzije i pozicija polja
        label.setBackground(boja);                                              // uvezena boja polja
        label.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N            // font kojim se vrsi ispis
        label.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);        // pozicija teksta u polju
        label.setText(text);                                                    // tekst koji se prikazuje
        label.setToolTipText(tekstDuzi);
        label.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        label.setOpaque(true);
    }

    private static int odredjivanjeKursa ( String kursK ) 
          
        {
            int r = 0;
// Odredjivanje broja dece po kursu
            if ( kursK.equals("HF1") ) {
                brojHF1++;
                r = 1;
            } else {
            if ( kursK.equals("HF2") ) {
                brojHF2++;
                r = 2;
            } else {
            if ( kursK.equals("KB1") ) {
                brojKB1++;
                r = 3;
            } else {
            if ( kursK.equals("KB2") ) {
                brojKB2++;
                r = 4;
            } else {
            if ( kursK.equals("KB3") ) {
                brojKB3++;
                r = 5;
            } else {
            if ( kursK.equals("KB4") ) {
                brojKB4++;
                r = 6;
            } else {
            if ( kursK.equals("KB5") ) {
                brojKB5++;
                r = 7;
            } else {
            if ( kursK.equals("KEY") ) {
                brojKEY++;    
                r = 8;
            } else {
            if ( kursK.equals("PET") ) {
                brojPET++; 
                r = 9;
            } else {
            if ( kursK.equals("FCE") ) {
                brojFCE++; 
                r = 10;
            } else {
            if ( kursK.equals("B2") ) {
                brojB2++; 
                r = 11;
            } else {
            if ( kursK.equals("PRO") ) {
                brojPRO++; 
                r = 12;
            } else {
            if ( kursK.equals("NE") ) {
                brojNE++; 
                r = 13;
        }}}}}}}}}}}}}
        return r;
    }   
// -- metod za odredjivanje boje polja + odredjivanje teksta iznad polja --
    private static Color boja ( String kurs ) {
        Color boja = null;
        if ( kurs.equals( "HF1" ) ) { boja = new Color(255, 255, 200); 
                                    tekstDuzi = "Hippo and Friends 1"; } 
        if ( kurs.equals( "HF2" ) ) { boja = new Color(170, 170, 240); 
                                    tekstDuzi = "Hippo and Friends 2";}
        if ( kurs.equals( "KB1" ) ) { boja = new Color(195, 195, 145);
                                    tekstDuzi = "Kid's Box 1"; }
        if ( kurs.equals( "KB2" ) ) { boja = new Color(100, 200, 230); 
                                    tekstDuzi = "Kid's Box 2"; }
        if ( kurs.equals( "KB3" ) ) { boja = new Color(150, 255, 200); 
                                    tekstDuzi = "Kid's Box 3"; }
        if ( kurs.equals( "KB4" ) ) { boja = new Color( 70, 210, 150);
                                    tekstDuzi = "Kid's Box 4"; }
        if ( kurs.equals( "KB5" ) ) { boja = new Color(245, 180,  80); 
                                    tekstDuzi = "Kid's Box 5"; }
        if ( kurs.equals( "KB6" ) ) { boja = new Color(190, 130, 130); 
                                    tekstDuzi = "Kid's Box 6"; }
        if ( kurs.equals( "KEY" ) ) { boja = new Color(165, 195, 200); 
                                    tekstDuzi = "Key English Test"; }
        if ( kurs.equals( "PET" ) ) { boja = new Color(200, 220,  60); 
                                    tekstDuzi = "Preliminary English Test"; }
        if ( kurs.equals( "FCE" ) ) { boja = new Color(250, 230, 115); 
                                    tekstDuzi = "First Certificate in English"; }
        if ( kurs.equals( "PRO" ) ) { boja = new Color(230, 205, 165); 
                                    tekstDuzi = "Profesional"; }
        if ( kurs.equals( "B2"  ) ) { boja = new Color( 30, 110, 100); 
                                    tekstDuzi = "B2 Level"; }
        if ( kurs.equals( "NEM" ) ) { boja = new Color( 50, 140, 190); 
                                    tekstDuzi = "Deutsch"; }
        return boja;
    }
// ^^ metod za odredjivanje boje polja ^^
    
}
